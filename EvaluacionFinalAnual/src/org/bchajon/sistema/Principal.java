package org.bchajon.sistema;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import org.bchajon.controller.MaestroController;
import org.bchajon.controller.MenuPrincipalController;
import org.bchajon.report.GenerarReporte;

public class Principal extends Application {
    private final String PAQUETE_VISTA = "/org/bchajon/view/";
    private Stage escenarioPrincipal;
    private Scene escena;
    
    @Override
    public void start(Stage escenarioPrincipal) {   
        this.escenarioPrincipal = escenarioPrincipal;
        escenarioPrincipal.setTitle("Final Anual 2019");
        menuPrincipal();
        escenarioPrincipal.show();
    }
    
    public void menuPrincipal(){
        try{
            MenuPrincipalController menuPrincipal = (MenuPrincipalController)cambiarEscena("MenuPrincipalView.fxml",606,225);
            menuPrincipal.setEscenarioPrincipal(this);
            
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void ventanaMaestro(){
        try{
            MaestroController maestroController = (MaestroController)cambiarEscena("MaestroView.fxml",932,517);
            maestroController.setEscenarioPrincipal(this);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void generarReporte(){
        imprimirReporte();
    }
    
    public void imprimirReporte(){
        Map parametros = new HashMap();
        parametros.put("Asignatura", JOptionPane.showInputDialog("Asignatura"));
        parametros.put("Grado", JOptionPane.showInputDialog("Grado"));
        parametros.put("Seccion", JOptionPane.showInputDialog("Seccion"));
        GenerarReporte.mostrarReporte("ReporteTaller.jasper", "Reporte de Taller", parametros);
    }
    
    public Initializable cambiarEscena(String fxml, int ancho, int alto) throws Exception{
        Initializable resultado = null;
        FXMLLoader cargadorFXML = new FXMLLoader();
        InputStream archivo = Principal.class.getResourceAsStream(PAQUETE_VISTA+fxml);
        cargadorFXML.setBuilderFactory(new JavaFXBuilderFactory());
        cargadorFXML.setLocation(Principal.class.getResource(PAQUETE_VISTA+fxml));
        escena = new Scene((AnchorPane)cargadorFXML.load(archivo),ancho,alto);
        escenarioPrincipal.setScene(escena);
        escenarioPrincipal.sizeToScene();
        resultado = (Initializable)cargadorFXML.getController();
        
        return resultado;
    }   
    
    public static void main(String[] args) {
        launch(args);
    }
}

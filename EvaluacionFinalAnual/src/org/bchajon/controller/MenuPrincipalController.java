package org.bchajon.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import org.bchajon.sistema.Principal;

public class MenuPrincipalController implements Initializable{
private Principal escenarioPrincipal;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public Principal getEscenarioPrincipal() {
        return escenarioPrincipal;
    }

    public void setEscenarioPrincipal(Principal escenarioPrincipal) {
        this.escenarioPrincipal = escenarioPrincipal;
    }
    
    public void ventanaMaestro(){
        escenarioPrincipal.ventanaMaestro();
    }
    
    public void generarReporte(){
        escenarioPrincipal.generarReporte();
    }
}

package org.bchajon.controller;

import eu.schudt.javafx.controls.calendar.DatePicker;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javax.swing.JOptionPane;
import org.bchajon.bean.Maestro;
import org.bchajon.db.Conexion;
import org.bchajon.sistema.Principal;

public class MaestroController implements Initializable{
    private Principal escenarioPrincipal;
    private enum operaciones{NUEVO, GUARDAR, EDITAR, NINGUNO, ACTUALIZAR, ELIMINAR, CANCELAR}
    private operaciones tipoDeOperacion = operaciones.NINGUNO;
    private ObservableList<Maestro> listaMaestro;
    private DatePicker fecha;
    @FXML private TextField txtNombreMaestro;
    @FXML private TextField txtApellidosMaestro;
    @FXML private ComboBox cmbSexo;
    @FXML private TextField txtTitulo;
    @FXML private TextField txtDireccion;
    @FXML private TextField txtTelefono;
    @FXML private GridPane grpFechaNacimiento;
    @FXML private TableView tblMaestros;
    @FXML private TableColumn colCodigoMaestro;
    @FXML private TableColumn colNombreMaestro;
    @FXML private TableColumn colApellidosMaestro;
    @FXML private TableColumn colFechaNacimiento;
    @FXML private TableColumn colSexo;
    @FXML private TableColumn colTitulo;
    @FXML private TableColumn colDireccion;
    @FXML private TableColumn colTelefono;
    @FXML private Button btnNuevo;
    @FXML private Button btnEliminar;
    @FXML private Button btnEditar;
    @FXML private Button btnReporte;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cargarDatos();
        desactivarControles();
        fecha = new DatePicker(Locale.ENGLISH);
        fecha.setDateFormat(new SimpleDateFormat("yyy-MM-dd"));
        fecha.getCalendarView().setShowWeeks(false);
        fecha.getCalendarView().todayButtonTextProperty().set("Today");
        fecha.getStylesheets().add("/org/bchajon/resources/DatePicker.css");
        grpFechaNacimiento.add(fecha, 0, 0);
        cmbSexo.setItems(FXCollections.observableArrayList("M", "F"));
    }
    
    public void cargarDatos(){
        tblMaestros.setItems(getMaestro());
        colCodigoMaestro.setCellValueFactory(new PropertyValueFactory<Maestro, Integer>("codigoMaestro"));
        colNombreMaestro.setCellValueFactory(new PropertyValueFactory<Maestro, String>("nombreMaestro"));
        colApellidosMaestro.setCellValueFactory(new PropertyValueFactory<Maestro, String>("apellidosMaestro"));
        colFechaNacimiento.setCellValueFactory(new PropertyValueFactory<Maestro, Date>("fechaNacimiento"));
        colSexo.setCellValueFactory(new PropertyValueFactory<Maestro, Date>("fechaNacimiento"));
        colSexo.setCellValueFactory(new PropertyValueFactory<Maestro, String>("sexo"));
        colTitulo.setCellValueFactory(new PropertyValueFactory<Maestro, String>("titulo"));
        colDireccion.setCellValueFactory(new PropertyValueFactory<Maestro, String>("direccion"));
        colTelefono.setCellValueFactory(new PropertyValueFactory<Maestro, String>("telefono"));
    }
    
    public ObservableList<Maestro> getMaestro(){
        ArrayList<Maestro> lista = new ArrayList<Maestro>();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call sp_ListarMaestros}");
            ResultSet resultado = procedimiento.executeQuery();
            while(resultado.next()){
                lista.add(new Maestro(resultado.getInt("codigoMaestro"),
                resultado.getString("nombreMaestro"),
                resultado.getString("apellidosMaestro"),
                resultado.getDate("fechaNacimiento"),
                resultado.getString("sexo"),
                resultado.getString("titulo"),
                resultado.getString("direccion"),
                resultado.getString("telefono")));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return listaMaestro = FXCollections.observableList(lista);
    }
    
    public Maestro buscarMaestro(int codigoMaestro){
        Maestro resultado = new Maestro();
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call sp_BuscarMaestro(?)}");
            procedimiento.setInt(1, codigoMaestro);
            ResultSet registro = procedimiento.executeQuery();
            while(registro.next()){
                resultado = new Maestro(registro.getInt("codigoMaestro"),
                registro.getString("nombreMaestro"),
                registro.getString("apellidosMaestro"),
                registro.getDate("fechaNacimiento"),
                registro.getString("sexo"),
                registro.getString("titulo"),
                registro.getString("direccion"),
                registro.getString("telefono"));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return resultado;
    }
    
    public void seleccionarElemento(){
        if(tblMaestros.getSelectionModel().getSelectedItem() != null) {
            txtNombreMaestro.setText(String.valueOf(((Maestro)tblMaestros.getSelectionModel().getSelectedItem()).getNombreMaestro()));
            txtApellidosMaestro.setText(String.valueOf(((Maestro)tblMaestros.getSelectionModel().getSelectedItem()).getApellidosMaestro()));
            fecha.setSelectedDate(((Maestro)tblMaestros.getSelectionModel().getSelectedItem()).getFechaNacimiento());
            cmbSexo.getSelectionModel().select(((Maestro)tblMaestros.getSelectionModel().getSelectedItem()).getSexo());
            txtTitulo.setText(String.valueOf(((Maestro)tblMaestros.getSelectionModel().getSelectedItem()).getTitulo()));
            txtDireccion.setText(String.valueOf(((Maestro)tblMaestros.getSelectionModel().getSelectedItem()).getDireccion()));
            txtTelefono.setText(String.valueOf(((Maestro)tblMaestros.getSelectionModel().getSelectedItem()).getTelefono()));
        }
    }
    
    public void nuevo(){
        switch(tipoDeOperacion){
            case NINGUNO:
                limpiarControles();
                activarControles();
                tblMaestros.setDisable(true);
                btnNuevo.setText("Guardar");
                btnEliminar.setText("Cancelar");
                btnEditar.setDisable(true);
                btnReporte.setDisable(true);
                tipoDeOperacion = operaciones.GUARDAR;
                break;
            case GUARDAR:
                if(txtNombreMaestro.getText().equals("") || txtApellidosMaestro.getText().equals("") ||
                        fecha.selectedDateProperty().get() == null || cmbSexo.getSelectionModel().getSelectedItem() == null
                        || txtTitulo.getText().equals("") || txtDireccion.getText().equals("") || txtTelefono.getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Deve llenar todos los campos");
                }else if(txtTelefono.getText().length()> 8){
                    JOptionPane.showMessageDialog(null, "Error: Exedio el número de digitos en Telefono");
                } 
                else{
                guardar();
                desactivarControles();
                tblMaestros.setDisable(false);
                btnNuevo.setText("Nuevo");
                btnEliminar.setText("Eliminar");
                btnEditar.setDisable(false);
                btnReporte.setDisable(false);
                tipoDeOperacion = operaciones.NINGUNO;
                limpiarControles();
                cargarDatos();
                break;
        }
    }
}
    public void eliminar(){
        switch(tipoDeOperacion){
            case GUARDAR:
                desactivarControles();
                tblMaestros.setDisable(false);
                btnNuevo.setText("Nuevo");
                btnEliminar.setText("Eliminar");
                btnEditar.setDisable(false);
                btnReporte.setDisable(false);
                tipoDeOperacion = operaciones.NINGUNO;
                limpiarControles();
                break;
            default:
                if(tblMaestros.getSelectionModel().getSelectedItem() != null){
                    int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de que desea eliminar el registro?", "Eliminar Maestro", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION){
                        try{
                            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call sp_EliminarMaestro(?)}");
                            procedimiento.setInt(1, ((Maestro)tblMaestros.getSelectionModel().getSelectedItem()).getCodigoMaestro());
                            procedimiento.execute();
                            listaMaestro.remove(tblMaestros.getSelectionModel().getSelectedIndex());
                            tblMaestros.getSelectionModel().select(null);
                            limpiarControles();
                            cmbSexo.setDisable(true);
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    }else{
                        limpiarControles();
                        desactivarControles();
                        cmbSexo.setDisable(true);
                        tblMaestros.getSelectionModel().select(null);
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un elemento.");
                }
                
        }
    }
    
    public void editar(){
        switch(tipoDeOperacion){
            case ACTUALIZAR:
                actualizar();
                desactivarControles();
                tblMaestros.setDisable(false);
                btnEditar.setText("Editar");
                btnReporte.setText("Reporte");
                btnNuevo.setDisable(false);
                btnEliminar.setDisable(false);
                tipoDeOperacion = operaciones.NINGUNO;
                limpiarControles();
                cargarDatos();
                break;
            case NINGUNO:
                if(tblMaestros.getSelectionModel().getSelectedItem()!=null){
                activarControles();
                tblMaestros.setDisable(true);
                btnEditar.setText("Actualizar");
                btnReporte.setText("Cancelar");
                btnNuevo.setDisable(true);
                btnEliminar.setDisable(true);
                cmbSexo.setDisable(true);
                grpFechaNacimiento.setDisable(true);
                tipoDeOperacion = operaciones.ACTUALIZAR;
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un elemento.");
                }break;
        }
    }
    
    public void generarReporte(){
        switch(tipoDeOperacion){
            case NINGUNO:
                limpiarControles();   
                JOptionPane.showMessageDialog(null, "No hay archivos disponibles.");
                break;
            case ACTUALIZAR:
                desactivarControles();
                tblMaestros.setDisable(false);
                btnEditar.setText("Editar");
                btnReporte.setText("Reporte");
                btnNuevo.setDisable(false);
                btnEliminar.setDisable(false);
                tipoDeOperacion = operaciones.NINGUNO;
                limpiarControles();
                break;
        }
    }
    
    public void actualizar(){
            Maestro registro = new Maestro();
            registro = (Maestro)tblMaestros.getSelectionModel().getSelectedItem();
            try{
                PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call sp_ActualizarMaestro(?,?,?,?,?,?,?,?)}");
                procedimiento.setInt(1, registro.getCodigoMaestro());
                procedimiento.setString(2, txtNombreMaestro.getText());
                procedimiento.setString(3, txtApellidosMaestro.getText());
                procedimiento.setDate(4, new java.sql.Date(fecha.getSelectedDate().getTime()));
                procedimiento.setString(5, cmbSexo.getSelectionModel().getSelectedItem().toString());
                procedimiento.setString(6, txtTitulo.getText());
                procedimiento.setString(7, txtDireccion.getText());
                procedimiento.setString(8, txtTelefono.getText());
                procedimiento.execute();
                listaMaestro.add(registro);
            }catch(Exception e){
                e.printStackTrace();
            }
    }
    
    public void guardar(){
        Maestro registro = new Maestro();
        registro.setNombreMaestro(txtNombreMaestro.getText());
        registro.setApellidosMaestro(txtApellidosMaestro.getText());
        registro.setFechaNacimiento(new java.sql.Date(fecha.getSelectedDate().getTime()));
        registro.setSexo(cmbSexo.getSelectionModel().getSelectedItem().toString());
        registro.setTitulo(txtTitulo.getText());
        registro.setDireccion(txtDireccion.getText());
        registro.setTelefono(txtTelefono.getText());
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call sp_AgregarMaestro(?,?,?,?,?,?,?)}");
            procedimiento.setString(1, registro.getNombreMaestro());
            procedimiento.setString(2, registro.getApellidosMaestro());
            procedimiento.setDate(3, new java.sql.Date(registro.getFechaNacimiento().getTime()));
            procedimiento.setString(4, registro.getSexo());
            procedimiento.setString(5, registro.getTitulo());
            procedimiento.setString(6, registro.getDireccion());
            procedimiento.setString(7, registro.getTelefono());
            procedimiento.execute();
            listaMaestro.add(registro);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void limpiarControles(){
        txtNombreMaestro.setText("");
        txtApellidosMaestro.setText("");
        cmbSexo.getSelectionModel().select(null);
        fecha.selectedDateProperty().set(null);
        txtTitulo.setText("");
        txtDireccion.setText("");
        txtTelefono.setText("");   
    }
    
    public void activarControles(){
        txtNombreMaestro.setDisable(false);
        txtApellidosMaestro.setDisable(false);
        cmbSexo.setDisable(false);
        grpFechaNacimiento.setDisable(false);
        txtTitulo.setDisable(false);
        txtDireccion.setDisable(false);
        txtTelefono.setDisable(false);
    }
    
    public void desactivarControles(){
        txtNombreMaestro.setDisable(true);
        txtApellidosMaestro.setDisable(true);
        cmbSexo.setDisable(true);
        grpFechaNacimiento.setDisable(true);
        txtTitulo.setDisable(true);
        txtDireccion.setDisable(true);
        txtTelefono.setDisable(true);
    }

    public Principal getEscenarioPrincipal() {
        return escenarioPrincipal;
    }

    public void setEscenarioPrincipal(Principal escenarioPrincipal) {
        this.escenarioPrincipal = escenarioPrincipal;
    }
    
    public void menuPrincipal(){
        escenarioPrincipal.menuPrincipal();
    }
}

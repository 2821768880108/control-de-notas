drop database if exists DBKinal2019;
Create database DBKinal2019;

use DBKinal2019;

Create table Maestros(
	codigoMaestro int auto_increment not null,
    nombreMaestro varchar(150) not null,
    apellidosMaestro varchar(150) not null,
    fechaNacimiento date not null,
    sexo varchar(1) not null,
    titulo varchar(150) not null,
    direccion varchar(250) not null,
    telefono varchar(9) not null,
    primary key PK_codigoMaestro(codigoMaestro)
);


Create table Grados(
	codigoGrado int auto_increment not null,
    grado varchar(15) not null,
    seccion varchar(5) not null,
    modalidad varchar(50) not null, -- presencial, semi-presencial y virtual
    jornada varchar(15) not null, -- Matutina, Vespertina, Nocturna
    primary key PK_codigoGrado (codigoGrado)
);

Create table Alumnos(
	codigoAlumno int auto_increment not null,
    nombresAlumno varchar(100) not null,
    apellidosAlumno varchar(100) not null,
    sexo varchar(1) not null,
    telefono varchar(8),
    direccion varchar(250) not null,
    email varchar(25) not null,
    nivel varchar(15) not null,
    codigoGrado int not null,
    primary key PK_codigoAlumno (codigoAlumno),
    constraint FK_Alumnos_Grados foreign key
		(codigoGrado) references Grados(codigoGrado)
);

Create table Asignaturas(
	codigoAsignatura int auto_increment not null,
    Asignatura varchar(100)not null,
    codigoGrado int not null,
    primary key PK_codigoAsignatura (codigoAsignatura),
    constraint FK_Asignaturas_Grados foreign key
		(codigoGrado) references Grados(codigoGrado)
);

Create table CalificacionBimestral(
	codigoBimestral int auto_increment not null,
    notaAcumulativa decimal(10,2) not null,
    notaEvaluacion decimal(10,2) not null,
    totalBimestre decimal(10,2) default 0,
    codigoAlumno int not null,
    codigoAsignatura int not null,
    primary key PK_codigoBimestral (codigoBimestral),
    constraint FK_CalificacionBimestral_Alumnos foreign key (codigoAlumno) references Alumnos(codigoAlumno),
    constraint FK_CalificacionBimestral_Asignaturas foreign key (codigoAsignatura) references Asignaturas(codigoAsignatura)    
);

Create table Grados_Maestros(
	codigoGrado int not null,
    codigoMaestro int not null,
    primary key (codigoGrado, codigoMaestro)
);

Create table Maestros_Asignaturas(
	codigoMaestro int not null,
    codigoAsignatura int not null,
    primary key (codigoMaestro, codigoAsignatura)    
);

----------------------------------------------------------------------------------------------------------------

-- Procedimiento De Agregar
Delimiter $$
Create Procedure sp_AgregarMaestro (in nombreMaestro Varchar(150), in apellidosMaestro Varchar(150), in fechaNacimiento Date, in sexo Varchar(1),
	in titulo Varchar(150), in direccion Varchar(250), in telefono Varchar(9))
	Begin
		Insert Into dbkinal2019.Maestros(nombreMaestro, apellidosMaestro, fechaNacimiento, sexo, titulo, direccion, telefono)
			Values(nombreMaestro, apellidosMaestro, fechaNacimiento, sexo, titulo, direccion, telefono);
	End$$
Delimiter ;

-- Procedimiento De Listar
Delimiter $$
Create Procedure sp_ListarMaestros()
	Begin
		Select
        Maestros.codigoMaestro,
        Maestros.nombreMaestro,
        Maestros.apellidosMaestro,
        Maestros.fechaNacimiento,
        Maestros.sexo,
        Maestros.titulo,
        Maestros.direccion,
        Maestros.telefono
        From DBKINAL2019.Maestros;
	End$$
Delimiter ;

 -- Procedimiento de Buscar
Delimiter $$
Create Procedure sp_BuscarMaestro(in codigo Int)
	Begin
		Select
        Maestros.codigoMaestro,
        Maestros.nombreMaestro,
        Maestros.apellidosMaestro,
        Maestros.fechaNacimiento,
        Maestros.sexo,
        Maestros.titulo,
        Maestros.direccion,
        Maestros.telefono
        From DBKINAL2019.Maestros Where codigoMaestro = codigo;
	End$$
Delimiter ;

 -- Procedimiento de Editar
Delimiter $$
Create Procedure sp_ActualizarMaestro(in codigo Int, in nombre Varchar(150), in apellidos Varchar(150), in fecha Date , in sexo Varchar(1),
	in titulo Varchar(150), in direccion Varchar(250), in telefono Varchar(9))
    Begin
		Update dbkinal2019.maestros Set nombreMaestro = nombre, apellidosMaestro = apellidos, fechaNacimiento = fecha, sexo = sexo,
			titulo = titulo, direccion = direccion, telefono = telefono Where codigoMaestro = codigo;
    End$$
Delimiter ;

-- Procedimiento De Eliminar
Delimiter $$
Create Procedure sp_EliminarMaestro(in codigo Int)
	Begin
		Delete From DBKINAL2019.Maestros Where codigoMaestro = codigo;
    End$$
Delimiter ;

-- FUNCION

Delimiter $$
Delimiter $$
Create function fn_calculoDeNota(cuarenta decimal(10,2), sesenta decimal(10,2)) 
	returns decimal(10,2)
	reads sql data deterministic 
	Begin 
		declare total decimal (10,2);
        set total = (cuarenta * 0.40) + (sesenta*0.60);
        return total;
	End$$
Delimiter ;

-- INSERT DE GRADOS
Insert into Grados(grado, seccion, modalidad, jornada)
	Values('Quinto Perito', 'PE5AM', 'Presencial','Matutina');

-- INSERT DE ASIGNATURAS
Insert into Asignaturas(asignatura, codigoGrado)
	Values('Taller', 1);
    
-- INSERT DE ALUMNOS 
Insert into Alumnos(nombresAlumno, apellidosAlumno, sexo, telefono, direccion, email, nivel, codigoGrado)
	Values('Brandon Alexander', 'Chajón España', 'M','12346879','6 calle','bchajon@yahoo.com','Medio', 1);
Insert into Alumnos(nombresAlumno, apellidosAlumno, sexo, telefono, direccion, email, nivel, codigoGrado)
	Values('Rudy Alexander', 'España Canel', 'M','98765432','8 av zona 8','Rudy8@gmail.com','Excelente', 1);
Insert into Alumnos(nombresAlumno, apellidosAlumno, sexo, telefono, direccion, email, nivel, codigoGrado)
	Values('Oscar Javier', 'Puac López', 'M','96325874','4 calle 5-79','OJavier@yahoo.com','Bajo', 1);
Insert into Alumnos(nombresAlumno, apellidosAlumno, sexo, telefono, direccion, email, nivel, codigoGrado)
	Values('Juan Antonio', 'Estrada Pineda', 'M','78531264','8 av zona 9 Mixco','Juan@yahoo.com','Medio', 1);
Insert into Alumnos(nombresAlumno, apellidosAlumno, sexo, telefono, direccion, email, nivel, codigoGrado)
	Values('Cristian Uriel', 'Jimenez Pirir', 'M','12368745','7 calle 9-4','bchajon@yahoo.com','Excelente', 1);
    
-- INSERT DE CALIFICACION BIMESTRAL    
Insert into dbkinal2019.calificacionbimestral(notaAcumulativa, notaEvaluacion, totalBimestre, codigoAlumno, codigoAsignatura)
	Values(80, 75, fn_NotaTotal(notaAcumulativa, notaEvaluacion), 1,1);
Insert into dbkinal2019.calificacionbimestral(notaAcumulativa, notaEvaluacion, totalBimestre, codigoAlumno, codigoAsignatura)
	Values(55, 100, fn_NotaTotal(notaAcumulativa, notaEvaluacion), 2,1);
Insert into dbkinal2019.calificacionbimestral(notaAcumulativa, notaEvaluacion, totalBimestre, codigoAlumno, codigoAsignatura)
	Values(45, 25, fn_NotaTotal(notaAcumulativa, notaEvaluacion), 3,1);
Insert into dbkinal2019.calificacionbimestral(notaAcumulativa, notaEvaluacion, totalBimestre, codigoAlumno, codigoAsignatura)
	Values(70, 90, fn_NotaTotal(notaAcumulativa, notaEvaluacion), 4,1);
Insert into dbkinal2019.calificacionbimestral(notaAcumulativa, notaEvaluacion, totalBimestre, codigoAlumno, codigoAsignatura)
	Values(80, 50, fn_NotaTotal(notaAcumulativa, notaEvaluacion), 5,1);

Delimiter $$
Create Procedure sp_ReporteCalificaciones(in asignatura Varchar(100), in grado Varchar(15), in seccion Varchar(5))
	Begin
		Select a.nombresAlumno, a.apellidosAlumno, asig.Asignatura, g.Grado, g.Seccion, cb.notaAcumulativa, cb.notaEvaluacion, cb.totalBimestre 
			From Asignaturas asig Inner Join Grados g On asig.codigoGrado = g.codigoGrado Inner Join Alumnos a On g.codigoGrado = a.codigoGrado
				Inner Join CalificacionBimestral cb On a.codigoAlumno = cb.codigoAlumno Where asig.Asignatura = asignatura And g.grado = grado And g.seccion = seccion; 
    End$$
Delimiter ;